import os
import threading
from config import *

for now in os.listdir(raw_folder):
    path = os.path.join(raw_folder, now)
    if path.endswith(".m4a") and not os.path.exists(path.replace(".m4a", ".mp3")):
        threading.Thread(target=os.system,
                         args=["ffmpeg -i \"%s\" \"%s\"" % (path, path.replace(".m4a", ".mp3"))]).start()
