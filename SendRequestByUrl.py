import requests
from config import *


def get_cookie():
    return open("cookie.txt", "r", encoding="UTF-8").read()


record_id = input("Input record ID:")
name = input("Input song name:")
url = input("Input URL:")
cookie = get_cookie()

edit_audio_req = requests.post("https://note.leke.cn/auth/pc/note/ajax/noteRecordEdit.htm", headers={
    "Cookie": cookie,
    "Host": "note.leke.cn",
    "User-Agent": user_agent}, data={
    "noteRecordId": record_id,
    "audioUrl": url,
    "content": name,
    "isImportant": False
})
print("Response:\n" + edit_audio_req.text)
