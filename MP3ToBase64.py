import os
import base64
from config import *


def ToBase64(file, txt):
    with open(file, 'rb') as fileObj:
        image_data = fileObj.read()
        base64_data = base64.b64encode(image_data)
        fout = open(txt, 'w')
        fout.write(base64_data.decode())
        fout.close()


def convert_folder(folder_path):
    for i in os.listdir(folder_path):
        if not i.endswith(".mp3"):
            continue
        file_path = folder_path + "\\" + i
        if os.path.exists(
                "base64\\" + file_path.replace("/", "\\").replace("C:\\Users\\cyrxdzj\\Music\\", "").replace(".mp3",
                                                                                                             ".txt")):
            continue
        ToBase64(file_path,
                 "base64\\" + file_path.replace("/", "\\").replace("C:\\Users\\cyrxdzj\\Music\\", "").replace(".mp3",
                                                                                                              ".txt"))
        print(file_path)


def convert_file(file_path):
    ToBase64(file_path, "base64\\" + file_path.split("\\")[-1].replace(".mp3", ".txt"))


if __name__ == "__main__":
    # convert_file(r"C:\Users\cyrxdzj\Music\VirtualSinger\时间日记.mp3")
    convert_folder(raw_folder)
