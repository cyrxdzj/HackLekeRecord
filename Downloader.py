from config import *
import requests
from tqdm import *

files = '''
Sad Sometimes|https://dl.stream.qqmusic.qq.com/C400004CfE1j0vDwvG.m4a?guid=3978905406&vkey=EC49727A2A63FED3998044449CD0B6AC61D2F72DB260E070BFDA98BCDAC0D9D0B5FBBE4189A02504427FD37FFC81930A881EFB748BAD3D32&uin=&fromtag=120032
岚 evergreen Live|https://dl.stream.qqmusic.qq.com/C400000sqZsn25wfPN.m4a?guid=4769181184&vkey=69ABF9887CE6A6C254E0041CF0130112BA50498752E497100297B709C0A8AB96781BA51B52FFD5A68740C8183ED7178AE7B953553719514F&uin=&fromtag=120032
驰 Timelapse|https://dl.stream.qqmusic.qq.com/C400002Fd8LK3oUOzs.m4a?guid=368704234&vkey=1C9BA548E41C09B72654FED190C84CCF1E75B0600DE8B5BF688D3BD9492DAEC283D58373BBF543A4348791A9AEA69C5B621D6981F5A4DED3&uin=&fromtag=120032
最后一页 ycccc|https://dl.stream.qqmusic.qq.com/C400002hGXnF2bAVgo.m4a?guid=7531757590&vkey=D569CD764A65FB7EA9AD4383688137BA725CB73D7013529DEBE4D9C69089D57CBF9A8EC778CCF026CCA173253B5C9F108294C18AA9BEC850&uin=&fromtag=120032
最后一页 Cor|https://dl.stream.qqmusic.qq.com/C400003wbcHo1Zj0Gc.m4a?guid=3050562150&vkey=3A2F4B47AF864CE72C8885A3E68546E4CA08004C4D7661D7A8C5DC94A0FAA3F8D11AA31B3D2E711529E47A48254049DD8F7891BF38AC3AF1&uin=&fromtag=120032
眼泪胜过谎言|https://dl.stream.qqmusic.qq.com/C400000AOzNw26NqQM.m4a?guid=2051373152&vkey=2BD7CE847D0AEBC69A317BAD29E791B05952E58E6304F99A10CA010FB27206C2789A549D6B6B84955D35D7E4235CD91DB0A3ACB4F5ED0A41&uin=&fromtag=120032
Lost Memory|https://dl.stream.qqmusic.qq.com/C400002XOulf42KRw3.m4a?guid=8666276398&vkey=51162128AE7011102A99D19778EEAB91E327E0CB733A889A478B17CA06141AF967672FB367502B441F242B2BCF31A5EF2F455287F8025E68&uin=&fromtag=120032
追光旅行|https://dl.stream.qqmusic.qq.com/C4000011r7a34UXczf.m4a?guid=6310098688&vkey=4887D7F454A610FA8273AABEFDE5F44E84BF993E2CF368268ED52D64FB999D558B0286651501A60156912224683B41182F8E3B039A854226&uin=&fromtag=120032
不将就|https://dl.stream.qqmusic.qq.com/C400001gu0b91KyQJy.m4a?guid=1113831168&vkey=15D36402F6729DC21FFE524C87C8FAB1C34E1A92266BE350F2651CF5B15EAA52C85145943D642721945591E6BE159E89AA550C5743430B5B&uin=&fromtag=120032
'''


def download(url, fname):
    print(url,fname)
    # 用流stream的方式获取url的数据
    resp = requests.get(url, stream=True)
    # 拿到文件的长度，并把total初始化为0
    total = int(resp.headers.get('content-length', 0))
    # 打开当前目录的fname文件(名字你来传入)
    # 初始化tqdm，传入总数，文件名等数据，接着就是写入，更新等操作了
    with open(fname, 'wb') as file, tqdm(
            desc=fname,
            total=total,
            unit='iB',
            unit_scale=True,
            unit_divisor=1024,
    ) as bar:
        for data in resp.iter_content(chunk_size=1024):
            size = file.write(data)
            bar.update(size)


if __name__ == "__main__":
    for now_file in files.split("\n"):
        if now_file.count("|") != 1:
            continue
        [song_name, url] = now_file.split("|")
        download(url, raw_folder + "/" + song_name + ".m4a")
