import requests
from config import *


def get_cookie():
    return open("cookie.txt", "r", encoding="UTF-8").read()


def get_base64_code():
    return open(single_file_path, "r", encoding="UTF-8").read()


cookie = get_cookie()
raw_file_content = open("uploaded-music.txt", "r", encoding="UTF-8").read()
if input("Will upload this music: " + single_file_path + "\nConfirm? Y/N:") != "Y":
    exit(0)
if single_file_path in raw_file_content:
    temp = input("Are you sure to upload a same audio? Y/N:")
    if temp != "Y":
        exit(0)
record_id = input("Input record ID:")
req = requests.post("https://note.leke.cn/auth/global/fs/upload/audio/base64.htm", headers={
    "Cookie": cookie,
    "Host": "note.leke.cn",
    "User-Agent": user_agent},
                    data={
                        "file": get_base64_code(),
                        "ext": "mp3",
                        "type": "audio"
                    })
print("Response 1:\n" + req.text)
response_object = req.json()
if response_object["success"]:
    with open("uploaded-music.txt", "w", encoding="UTF-8") as fobj:
        fobj.write(raw_file_content + "\n" + single_file_path + " " + response_object["datas"]["url"])
    name = single_file_path.split("\\")[-1].replace(".txt", "")
    edit_audio_req = requests.post("https://note.leke.cn/auth/pc/note/ajax/noteRecordEdit.htm", headers={
        "Cookie": cookie,
        "Host": "note.leke.cn",
        "User-Agent": user_agent},
                                   data={
                                       "noteRecordId": record_id,
                                       "audioUrl": response_object["datas"]["url"],
                                       "content": name,
                                       "isImportant": False
                                   })
    print("Response 2:\n" + edit_audio_req.text)
else:
    print("There was something wrong in request 1.")
